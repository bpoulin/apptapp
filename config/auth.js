// config/auth.js
require('dotenv').config()

// expose our config directly to our application using module.exports
module.exports = {

    'facebookAuth' : {
        'clientID'      : process.env.FACEBOOK_CLIENT_ID,
        'clientSecret'  : process.env.FACEBOOK_CLIENT_SECRET,
        'callbackURL'   : process.env.FACEBOOK_CALLBACK_URL,
    },

        'stravaAuth' : {
            'clientID'      : process.env.STRAVA_CLIENT_ID,
            'clientSecret'  : process.env.STRAVA_CLIENT_SECRET,
            'callbackURL'   : process.env.STRAVA_CALLBACK_URL,
            'token'         : process.env.STRAVA_TOKEN,
        },

    'googleAuth' : {
        'clientID'      : process.env.GOOGLE_CLIENT_ID,
        'clientSecret'  : process.env.GOOGLE_CLIENT_SECRET,
        'callbackURL'   : process.env.GOOGLE_CALLBACK_URL, 
    }

};
